# EB_INV_F

Table des données de codage des invalidités


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|INV_ATR_DTD|date|Date d'attribution de la pension d'invalidité|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|INV_PEN_CAT|nombre réel|Catégorie de la pension d'invalidité|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|INV_PEN_ETA|nombre réel|Etat de la pension d'invalidité|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
